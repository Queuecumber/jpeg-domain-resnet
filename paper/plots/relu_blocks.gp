#!/usr/bin/gnuplot -c

load "common.gp"
load "relu_styles.gp"

set size 1.0, 1.1

set key horizontal

set ylabel 'Average RMSE'
set ytics 0.1

set xlabel 'Number of Spatial Frequencies'
set xrange [1:15]
set xtics 1,2,15

set output "relu_blocks.eps"
plot "data/relu_blocks.csv" using ($0+1):2 with linespoints linestyle apx_block title columnhead, \
     "data/relu_blocks.csv" using ($0+1):1 with linespoints linestyle asm_block title columnhead
