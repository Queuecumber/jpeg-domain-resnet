set size 1.0, 1.0

# method colors
apx_color = "#e69f00"
asm_color = "#009e73"

# dataset point types
mnist_point = 7
cifar10_point = 9
cifar100_point = 5
block_point = 2

# spatial lines
set style line 1 linewidth 8 dashtype 2 linetype rgb "black"
spatial = 1

# mnist lines
set style line 2 linewidth 8 pointsize 4 pointtype mnist_point linecolor rgb asm_color
set style line 3 linewidth 8 pointsize 4 pointtype mnist_point linecolor rgb apx_color
asm_mnist = 2
apx_mnist = 3

# cifar10 lines
set style line 4 linewidth 8 pointsize 5 pointtype cifar10_point linecolor rgb asm_color
set style line 5 linewidth 8 pointsize 5 pointtype cifar10_point linecolor rgb apx_color
asm_cifar10 = 4
apx_cifar10 = 5

# cifar100 lines
set style line 6 linewidth 8 pointsize 4 pointtype cifar100_point linecolor rgb asm_color
set style line 7 linewidth 8 pointsize 4 pointtype cifar100_point linecolor rgb apx_color
asm_cifar100 = 6
apx_cifar100 = 7

# block lines
set style line 8 linewidth 8 pointsize 4 pointtype block_point linecolor rgb asm_color
set style line 9 linewidth 8 pointsize 4 pointtype block_point linecolor rgb apx_color
asm_block = 8
apx_block = 9