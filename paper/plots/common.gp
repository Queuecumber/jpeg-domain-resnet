# common settings
set terminal postscript enhanced color font "Helvetica, 30" eps
set size 1.2, 1.2

set palette rgb 33,13,10

set datafile separator ','

set grid ytics

set key autotitle columnhead

unset colorbox

set key above vertical maxrows 3