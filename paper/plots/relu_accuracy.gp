#!/usr/bin/gnuplot -c

load "common.gp"
load "relu_styles.gp"

set size 1.0, 1.2

set ylabel 'Average Accuracy (%)'
set ytics 0.2

set xlabel 'Number of Spatial Frequencies'
set xrange [1:15]
set xtics 1,2,15

set output "relu_accuracy.eps"
plot "data/MNIST_relu_accuracy.csv" using ($0+1):1 with lines linestyle spatial notitle, \
     "data/CIFAR10_relu_accuracy.csv" using ($0+1):1 with lines linestyle spatial notitle, \
     "data/CIFAR100_relu_accuracy.csv" using ($0+1):1 with lines linestyle spatial notitle, \
     "data/MNIST_relu_accuracy.csv" using ($0+1):3 with linespoints linestyle apx_mnist title (columnhead(3)." MNIST"), \
     "data/CIFAR10_relu_accuracy.csv" using ($0+1):3 with linespoints linestyle apx_cifar10 title (columnhead(3)." CIFAR10"), \
     "data/CIFAR100_relu_accuracy.csv" using ($0+1):3 with linespoints linestyle apx_cifar100 title (columnhead(3)." CIFAR100"), \
     "data/MNIST_relu_accuracy.csv" using ($0+1):2 with linespoints linestyle asm_mnist title (columnhead(2)." MNIST"), \
     "data/CIFAR10_relu_accuracy.csv" using ($0+1):2 with linespoints linestyle asm_cifar10 title (columnhead(2)." CIFAR10"), \
     "data/CIFAR100_relu_accuracy.csv" using ($0+1):2 with linespoints linestyle asm_cifar100 title (columnhead(2)." CIFAR100")