#!/usr/bin/gnuplot -c

load "common.gp"

set boxwidth 0.5
set style fill solid

set yrange [0:20]
set ylabel 'Throughput (Images/Sec)'

set style data histogram
set style histogram cluster gap 1
set boxwidth 0.9

set xtics scale 0

set output "throughput.eps"
plot "data/throughput.csv" using 2:xtic(1), \
     "data/throughput.csv" using 3, \
     "data/throughput.csv" using 4, \
     "data/throughput.csv" using 5

