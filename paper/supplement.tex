\documentclass[10pt,twocolumn,letterpaper]{article}

\usepackage{iccv}

\input{packages}

% Include other packages here, before hyperref.

% If you comment hyperref and then uncomment it, you should delete
% egpaper.aux before re-running latex.  (Or just hit 'q' on the first latex
% run, let it finish, and you should be clear).
\usepackage[breaklinks=true,bookmarks=false]{hyperref}

\iccvfinalcopy % *** Uncomment this line for the final submission

\def\iccvPaperID{****} % *** Enter the ICCV Paper ID here
\def\httilde{\mbox{\tt\raisebox{-.5ex}{\symbol{126}}}}

% Pages are numbered in submission mode, and unnumbered in camera-ready
%\ificcvfinal\pagestyle{empty}\fi

\addbibresource{bibliography.bib}

\DeclareCaptionFormat{algor}{%
    \hrulefill\par\offinterlineskip\vskip1pt%
    \textbf{#1#2}#3\offinterlineskip\hrulefill}
\DeclareCaptionStyle{algori}{singlelinecheck=off,format=algor,labelsep=space}
\captionsetup[algorithm]{style=algori}

\begin{document}

\title{Supplementary Material}

\maketitle

\section{Proof of the DCT Least Squares Approximation Theorem}

\begin{theorem}[DCT Least Squares Approximation Theorem]
    Given a set of $N$ samples of a signal $X = \{x_0, ... x_N\}$, let $Y = \{y_0, ... y_N\}$ be the DCT coefficients of $X$. Then, for any $1 \leq m \leq N$, the approximation
    
    \begin{equation}
    p_m(t) = \frac{1}{\sqrt{n}}y_o + \sqrt{\frac{2}{n}}\sum_{k=1}^{m} y_k\cos\left(\frac{k(2t + 1)\pi}{2n}\right)
    \label{eq:dct1d}
    \end{equation}
    of $X$ minimizes the least squared error
    \begin{equation}
    e_m = \sum_{i=0}^{n} (p_m(i) - x_i)^2
    \end{equation}
    \label{thm:dctls}
\end{theorem}
\begin{proof}
First consider that since Equation \ref{eq:dct1d} represents the Discrete Cosine Transform, which is a Linear map, we can write rewrite it as 
\begin{equation}
D^T_my = x
\end{equation}
where $D_m$ is formed from the first $m$ rows of the DCT matrix, $y$ is a row vector of the DCT coefficients, and $x$ is a row vector of the original samples.

To solve for the least squares solution, we use the the normal equations, that is we solve
\begin{equation}
    D_mD^T_my = D_mx
\end{equation}
and since the DCT is an orthonormal transformation, the rows of $D_m$ are orthogonal, so $D_mD^T_m = I$. Therefore
\begin{equation}
    y = D_mx
\end{equation}
Since there is no contradiction, the least squares solution must use the first $m$ DCT coefficients.
\end{proof}

\section{Proof of the DCT Mean-Variance Theorem}

\begin{theorem}[DCT Mean-Variance Theorem]
    Given a set of samples of a signal $X$ such that $\e[X] = 0$, let $Y$ be the DCT coefficients of $X$. Then 
    \begin{equation}
    \var[X] = \e[Y^2]
    \end{equation}
\end{theorem}
\begin{proof}
Start by considering $\var[X]$. We can rewrite this as
\begin{equation}
\var[X] = \e[X^2] - \e[X]^2
\end{equation}
Since we are given $\e[X] = 0$, this simplifies to 
\begin{equation}
\var[X] = \e[X^2]
\end{equation}
Next, we express the DCT as a linear map such that $X = DY$ and rewrite the previous equation as
\begin{equation}
\var[X] = \e[(DY)^2]
\end{equation}
Distributing the squaring operation gives
\begin{equation}
\e[(DY)^2] = \e[(D^TD)Y^2]
\end{equation}
Since $D$ is orthogonal this simplifies to
\begin{equation}
\e[(D^TD)Y^2] = \e[(D^{-1}D)Y^2] = \e[Y^2]
\end{equation}
\end{proof}

\section{Algorithms}

We conclude by outlining in pseudocode the algorithms for the three layer operations described in the paper. Algorithm \ref{alg:dce} gives the code for convolution explosion, Algorithm \ref{alg:asmr} gives the code for the ASM ReLu approximation, and Algorithm \ref{alg:bn} gives the code for Batch Normalization.

\captionof{algorithm}{Convolution Explosion. $K$ is an initial filter, $m, n$ are the input and output channels, $h, w$ are the image height and width, $s$ is the stride, $\star_s$ denotes the discrete convolution with stride $s$}
\label{alg:dce}
\begin{algorithmic}
    \Function{Explode}{$K, m, n, h, w, s$} 
    \State $d_j \gets \mathbf{shape}(\widetilde{J})$
    \State $d_b \gets (d_j[0], d_j[1], d_j[2], 1, h, w)$
    \State $\widehat{J} \gets \mathbf{reshape}(\widetilde{J},d_b)$
    \State $\widehat{C} \gets \widehat{J} \star_s K$
    \State $d_c \gets (m, n, d_j[0], d_j[1], d_j[2], h/s, h/s)$
    \State $\widetilde{C} \gets \mathbf{reshape}(\widehat{C}, d_c)$
    \State $\mathbf{return} \; \widetilde{C}J$
    \EndFunction
\end{algorithmic}

\newpage

\captionof{algorithm}{Approximated Spatial Masking for ReLu. $F$ is a DCT domain block, $\phi$ is the desired maximum spatial frequencies, $N$ is the block size.}
\label{alg:asmr}
\begin{algorithmic}
    \Function{ReLu}{$F, \phi, N$} 
    \State $M \gets$ \Call{ANNM}{$F, \phi, N$}
    \State $\mathbf{return}\;$ \Call{ApplyMask}{$F, M$}
    \EndFunction
    \Function{ANNM}{$F, \phi, N$} 
    \State $I \gets \mathbf{zeros}(N, N)$
    
    \For{$i \in [0, N)$}
    \For{$j \in [0, N)$}
    \For{$\alpha \in [0, N)$}
    \For{$\beta \in [0, N)$}
    \If{$\alpha + \beta \leq \phi$}
    \State $I_{ij} \gets I_{ij} + F_{ij}D^{\alpha\beta}_{ij}$
    \EndIf
    \EndFor
    \EndFor                
    \EndFor
    \EndFor
    
    \State $M \gets \mathbf{zeros}(N, N)$
    \State $M[I > 0] \gets 1$
    \State $\mathbf{return} \; M$
    
    \EndFunction
    \Function{ApplyMask}{$F, M$} 
    \State $\mathbf{return} \; H^{\alpha\beta ij}_{\alpha'\beta'}F_{\alpha\beta}M_{ij}$
    \EndFunction
\end{algorithmic}

\captionof{algorithm}{Batch Normalization. $F$ is a batch of JPEG blocks (dimensions $N \times 64$), $S$ is the inverse quantization matrix, $m$ is the momentum for updating running statistics, $t$ is a flag that denotes training or testing mode. The parameters $\gamma$ and $\beta$ are stored externally to the function. $\widehat{}\;$ is used to denote a batch statistic and $\tilde{}\;$ is used to denote a running statistic.}
\label{alg:bn}
\begin{algorithmic}
    \Function{BatchNorm}{$F$,$S$,$m$,$t$}
        \If{$t$}
            \State $\mu \gets \mathbf{mean}(F[:, 0])$
            \State $\widehat{\mu} \gets F[:, 0]$
            \State $F[:, 0] = 0$
            \State $D_g \gets F_kS_k$
            \State $\widehat{\sigma^2} \gets \mathbf{mean}(F^2, 1)$
            \State $\sigma^2 \gets \mathbf{mean}(\widehat{\sigma^2} + \widehat{\mu}^2) - \mu^2$
            \State $\widetilde{\mu} \gets \widetilde{\mu}(1 - m) + \mu m$
            \State $\widetilde{\sigma^2} \gets \widetilde{\sigma^2}(1 - m) + \mu m$
            \State $F[:, 0] \gets F[:, 0] - \mu$
            \State $F \gets \frac{\gamma F}{\sigma}$
            \State $F[:, 0] \gets  F[:, 0] + \beta$
        \Else
            \State $F[:, 0] \gets F[:, 0] - \widetilde{\mu}$
            \State $F \gets \frac{\gamma F}{\widetilde{\sigma}}$
            \State $F[:, 0] \gets F[:, 0] + \beta$
        \EndIf
        \State $\mathbf{return} \; F$
    \EndFunction
\end{algorithmic}

\end{document}
