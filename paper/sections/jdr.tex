\section{JPEG Domain Residual Networks}

The ResNet architecture, consists of blocks of four basic operations: Convolution (potentially strided), ReLu, Batch Normalization, and Component-wise addition, with the blocks terminating with a global average pooling operation \cite{he2016deep} before a fully connected layer performs the final classification. Our goal will be to develop JPEG domain equivalents to these five operations. Network activations are given as a single tensor holding a batch of multi-channel images, that is $I \in N^* \otimes P^* \otimes H^* \otimes W^*$.

\subsection{Convolution}

The convolution operation follows directly from the discussion in Section \ref{sec:backjlm}. The convolution operation in the spatial domain is a shorthand notation for a linear map $C: N^* \otimes P^* \otimes H^* \otimes W^* \rightarrow N^* \otimes P^* \otimes H^* \otimes W^*$. Since the same operation is applied to each image in the batch, we can represent $C$ with a type (3, 3) tensor. The entries of this tensor give the coefficient for a given pixel in a given input channel for each pixel in each output channel. We now develop the algorithm for representing discrete convolutional filters using this data structure.

A naive algorithm can simply copy randomly initialized convolution weights into this larger structure following the formula for convolution and then apply the JPEG compression and decompression tensors to the result. However, this is difficult to parallelize and incurs additional memory overhead to store the spatial domain operation. A more efficient algorithm would produce the JPEG domain operation directly and be easy to express as a compute kernel for a GPU. Start by considering the JPEG decompression tensor $\widetilde{J}$. Note that since $\widetilde{J} \in X \otimes Y \otimes K \otimes H^* \otimes W^*$ the last two indices of $\widetilde{J}$ form single channel image under our image model (\eg the last two indices are in $H^* \otimes W^*$). If the convolution can be applied to this "image", then the resulting map would decompress and convolve simultaneously. We can formulate a new tensor $\widehat{J} \in N \otimes H^* \otimes W^*$
by reshaping $\widetilde{J}$ and treating this as a batch of images \footnote{Consider as a concrete example using $32 \times 32$ images. Then $\widetilde{J}$ is of shape $4 \times 4 \times 64 \times 32 \times 32$ and the described reshaping gives $\widehat{J}$ of shape $1024 \times 1 \times 32 \times 32$ which can be treated as a batch of size 1024 of $32 \times 32$ images for convolution.}. Then, given randomly initialized filter weights, $K$ computing 
\begin{equation}
\label{eq:sdtojdc}
\widehat{C}^b = K \star \widehat{J}^b 
\end{equation}
where $\star$ indicates the convolution operation and $\widehat{J}^b$ indexes $\widehat{J}$ in the batch dimension, gives us the desired map. After reshaping $\widehat{C}$ back to the original shape of $\widetilde{J}$ to give $\widetilde{C}$, the full compressed domain operation can be expressed as 
\begin{equation}
\Xi^{pxyk}_{p'x'y'k'} = \widetilde{C}^{pxyk}_{p'hw}J^{hw}_{x'y'k'}
\end{equation}
where $p$ and $p'$ index the input and output channels of the image respectively. This algorithm skips the overhead of computing the spatial domain map explicitly and depends only on the batch convolution operation which is available in all GPU accelerated deep learning libraries. Further, the map can be precomputed to speed up inference by avoiding repeated applications of the convolution. At training time, the gradient of the compression and decompression operators is computed and used to find the gradient of the original convolution filter with respect to the previous layers error, then the map $\Xi$ is updated using the new filter. So, while inference efficiency of the convolution operation is greatly improved, training efficiency is limited by the more complex update. We show in Section \ref{sec:expeff} that the training throughput is still higher than the equivalent spatial domain model.

\subsection{ReLu}

\begin{figure}
    \centering
    \begin{subfigure}{0.20\textwidth}
        \captionsetup{width=.8\linewidth}
        \centering
        \includegraphics[width=\textwidth]{figures/asm_relu_original.png}
        \caption{Original image}
    \end{subfigure}%
    \begin{subfigure}{0.2\textwidth}
        \captionsetup{width=.8\linewidth}
        \centering
        \includegraphics[width=\textwidth]{figures/asm_relu_true.png}
        \caption{True ReLu}
    \end{subfigure}\\
    \begin{subfigure}{0.2\textwidth}
        \captionsetup{width=.8\linewidth}
        \centering
        \includegraphics[width=\textwidth]{figures/asm_relu_apx.png}
        \caption{ReLu using direct approximation}
    \end{subfigure}%
    \begin{subfigure}{0.2\textwidth}
        \captionsetup{width=.8\linewidth}
        \centering
        \includegraphics[width=\textwidth]{figures/asm_relu_asm.png}
        \caption{ReLu using ASM approximation}
    \end{subfigure}
    \caption{Example of ASM ReLu on an $8 \times 8$ block. Green pixels are negative, red pixels are positive, and blue pixels are zero. 6 spatial frequencies are used for both approximations. Note that the direct approximation fails to preserve positive pixel values.}
    \label{fig:asm}
\end{figure}

Computing ReLu in the JPEG domain is not as straightforward since ReLu is a non-linear function. Recall that the ReLu function is given by 

\begin{equation}
    r(x) = \begin{cases}
    x & x > 0 \\
    0 & x \leq 0
    \end{cases}
\end{equation}
We begin by defining the ReLu in the DCT domain and show how it can be trivially extended to the JPEG transform domain. To do this, we develop a general approximation technique called Approximated Spatial Masking that can apply any piecewise linear function to JPEG compressed images.

To develop this technique we must balance two seemingly competing criteria. The first is that we want to use the JPEG transform domain, since it has a computational advantage over the spatial domain. The second is that we want to compute a non-linear function which is incompatible with the JPEG transform. Can we balance these two constraints by sacrificing a third criterion? Consider an approximation of the spatial domain image that uses only a subset of the DCT coefficients. Computing this is fast, since it does not use the full set of coefficients, and gives us a spatial domain representation which is compatible with the non-linearity. What we sacrifice is accuracy. The accuracy-speed tradeoff is tunable to the problem by changing the size of the set of coefficients.

By Theorem \ref{thm:dctls} we use the lowest $m$ frequencies for an optimal reconstruction. For the $8 \times 8$ DCT used in the JPEG algorithm, this gives 15 spatial frequencies total (numbered 0 to 14). We can then fix a maximum number of spatial frequencies $k$ and use all coefficients $\phi$ such that $\phi \leq k$ as our approximation. 

If we now compute the piecewise linear function on this approximation directly there are two major problems. The first is that, although the form of the approximation is motivated by a least squares minimization, it is by no means guaranteed to reproduce the original values of \textit{any} of the pixels. The second is that this gives the value of the function in the spatial domain, and to continue using a JPEG domain network we would need to compress the result which adds computational overhead.

To solve the first problem we examine the intervals that the linear pieces fall into. The larger these intervals are, the more likely we are to have produced a value in the correct interval \footnote{For example if the original pixel value was 0.7 and the approximate value is 0.5, then the approximation is in the correct interval for ReLU ($\geq 0$) but its value is incorrect.} in our approximation. Further, since the lowest $k$ frequencies minimize the least squared error, the higher the frequency, the less likely it is to push a pixel value out of the correct range. With this motivation, we can produce a binary mask for each piece of the function. The linear pieces can then be applied directly to the DCT coefficients, and then multiplied by their masks and summed to give the final result. This preserves all pixel values. The only errors would be in the mask which would result in the wrong linear piece being applied. This is the fundamental idea behind the Approximated Spatial Masking (ASM) technique.

The final problem is that we now have a mask in the spatial domain, but the original image is in the DCT domain. There is a well known algorithm for pixelwise multiplication of two DCT images \cite{smith1993algorithms}, but it would require the mask to also be in the DCT domain. Fortunately, there is a straightforward solution that is a result of the multilinear analysis given in Section \ref{sec:backjlm}. Consider the bilinear map 
\begin{equation}
H: A^* \otimes B^* \times M^* \otimes N^* \rightarrow A^* \otimes B^*
\end{equation}
that takes a DCT block, $F$, and a spatial mask $G$, and produces the masked DCT block by pixelwise multiplication. Our task will be to derive the form of $H$. We proceed by construction. The steps of such an algorithm naively would be 
\begin{enumerate}
    \item Take the inverse DCT of F: $I_{mn} = D^{\alpha\beta}_{mn}F_{\alpha\beta}$
    \item Pixelwise multiply: $I'_{mn} = I_{mn}G_{mn}$
    \item Take the DCT of $I'$: $F'_{\alpha'\beta'} = D^{mn}_{\alpha'\beta'}I'_{mn}$.
\end{enumerate}
Since these three steps are linear or bilinear maps, they can be combined 
\begin{equation}
F'_{\alpha'\beta'} = F^{\alpha\beta}[D^{mn}_{\alpha\beta}D^{mn}_{\alpha'\beta'}]G_{mn}
\end{equation}
Giving the final bilinear map $H$ as 
\begin{equation}
H^{\alpha\beta mn}_{\alpha'\beta'} = D^{\alpha\beta mn}D^{mn}_{\alpha'\beta'}
\end{equation}

We call $H$ the Harmonic Mixing Tensor since it gives all the spatial frequency permutations that we need. $H$ can be precomputed to speed up computation.

To use this technique to compute the ReLu function, consider this alternative formulation
\newcommand{\nnm}{\mathrm{nnm}}

\begin{equation}
    \nnm(x) = \begin{cases}
    1 & x > 0 \\
    0 & x \leq 0
    \end{cases}
\end{equation}
We call the function $\nnm(x)$ the nonnegative mask of $x$. This is our binary mask for ASM. We express the ReLu function as 

\begin{equation}
    r(x) = \nnm(x)x
\end{equation}
This new function can be computed efficiently from fewer spatial frequencies with much higher accuracy since only the sign of the original function needs to be correct. Figure \ref{fig:asm} gives an example of this algorithm on a random block and compares it to computing ReLu on the approximation directly. Note that in the ASM image the pixel values of all positive pixels are preserved, the only errors are in the mask. In the direct approximation, however, none of the pixel values are preserved and it suffers from masking errors. The magnitude of the error is tested in Section \ref{sec:exprla} and pseudocode for the ASM algorithm is given in the supplementary material. 

To extend this method from the DCT domain to the JPEG transform domain, the rest of the missing JPEG tensor can simply be applied as follows:
\begin{equation}
H^{k mn}_{k'} = Z^{k}_{\gamma} \widetilde{S}^{\gamma}_{\alpha\beta} D^{\alpha\beta mn}D^{mn}_{\alpha'\beta'} S^{\alpha'\beta'}_{\gamma'} Z^{\gamma'}_{k'}
\end{equation}
Since the operation is the same for each block, and there are no interactions between blocks, the blocking tensor $B$ can be skipped.

\subsection{Batch Normalization}
\label{sec:jdrbn}

Batch normalization \cite{ioffe2015batch} has a simple and efficient formulation in the JPEG domain. Recall that batch normalization defines two learnable parameters: $\gamma$ and $\beta$. A given feature map $I$ is first centered and then normalized over the batch, then scaled by $\gamma$ and translated by $\beta$. The full formula is
\begin{equation}
\text{BN}(I) = \gamma\frac{I - \e[I]}{\sqrt{\var[I]}} + \beta
\end{equation}
So to define the batch normalization operation in the JPEG domain, we need four parts: the mean, the variance, scalar multiplication and scalar addition. Again, we first derive the result in the DCT domain and trivially extend to the JPEG transform domain.

We start with the sample mean. Observe, from the definition of the DCT, the first DCT coefficient is given by 
\begin{equation}
D_{00} = \frac{1}{2\sqrt{2N}}\sum_{x=0}^N\sum_{y=0}^N I_{xy}
\end{equation}
In other words, the (0,0) DCT coefficient is proportional to the mean of the block. Further, since the DCT basis is orthonormal, we can be sure that the remaining DCT coefficients do not depend on the mean. This means that to center the image we need only set the (0,0) DCT coefficient to 0. This, of course, is unaffected by the other steps of the JPEG transform. For tracking the running mean, we simply read this value. Note that this is a much more efficient operation than the mean computation in the spatial domain.

Next, to get the variance, we use the following theorem:
\begin{theorem}[The DCT Mean-Variance Theorem]
    Given a set of samples of a signal $X$ such that $\e[X] = 0$, let $Y$ be the DCT coefficients of $X$. Then 
    \begin{equation}
    \var[X] = \e[Y^2]
    \end{equation}
\end{theorem}
Intuitively this makes sense because the (0,0) coefficient represents the mean, the remaining DCT coefficients are essentially spatial oscillations around the mean, which should define the variance. Proof of this theorem is given in the supplementary material.

To apply $\gamma$ and the variance, we use scalar multiplication. This follows directly from the definition of the DCT (and JPEG) as a linear map
\begin{equation}
    J(\gamma I) = \gamma J(I)
\end{equation}
For scalar addition to apply $\beta$, note that since the (0,0) coefficient is the mean, and adding $\beta$ to every pixel in the image is equivalent to raising the mean by $\beta$, we can simply add $\beta$ to the (0,0) coefficient.

To extend this to JPEG is simple. The proportionality constant for the (0,0) coefficient is $\frac{1}{2\sqrt{2 \times 8}} = \frac{1}{8}$. For this reason, many quantization matrices use $8$ as the (0,0) quantization coefficient. This means that the 0th block entry for a block does not need any proportionality constant, it stores exactly the mean. So for adding $\beta$, we can simply set the 0th position to $\beta$ without performing additional operations. The other operations are unaffected. 

\subsection{Component-wise Addition}

Component-wise addition is the simplest formulation in our network. This is a well known result detailed in \cite{chang1992video, shen1998block, shen1995inner, smith1993algorithms} among others. Since the JPEG transform, $J$, is a linear map, for two images $F$ and $G$, we have 
\begin{equation}
J(F + G) = J(F) + J(G)
\end{equation}
meaning that we can simply perform a component-wise addition of the JPEG compressed results with no need for further processing.

\subsection{Global Average Pooling}

\begin{figure}
    \centering
    \includegraphics[width=0.5\linewidth]{figures/globalavgpooling.pdf}
    \caption{Global average pooling. The 0th coefficient of each block can be used directly with no computation.}
    \label{fig:gap}
\end{figure}

Global average pooling also has a simple formulation in JPEG domain. Recall from the discussion of Batch Normalization (Section \ref{sec:jdrbn}) that the 0th element of the block after quantization is equal to the mean of the block. Then this element can be extracted channel-wise from each block and the global average pooling result is the channel-wise mean of these elements.

Furthermore, our network architecture for classification will always reduce the input images to a single block, which can then have its mean extracted and reported as the global average pooling result directly. Note the efficiency of this process: rather than channel-wise averaging in a spatial domain network, we simply have an unconditional read operation, one per channel. This is illustrated in Figure \ref{fig:gap}.

\subsection{Model Conversion}

The previous sections described how to build the ResNet component operations in the JPEG domain. While this implies straightforward algorithms for both inference and learning on JPEGs, we can also convert pre-trained models for JPEG inference. This allows any model that was trained on spatial domain images to benefit from our algorithms at inference time. Consider Equation \ref{eq:sdtojdc}. In this equation, $K$ holds the randomly initialized convolution filter. By instead using pretrained spatial weights for $K$, the convoltion will work as expected on JPEGs. Similarly, pretrained $\alpha, \beta, \mu, \sigma$ for batch normalization can be provided. By doing this for each layer in a pretrained network, the network will operate on JPEGs. The only caveat is that the ReLu approximation accuracy can effect the final performance of the network since the weights were not trained to cope with it. This is tested in Section \ref{sec:exprla}.