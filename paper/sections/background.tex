\section{Background}

We briefly review the JPEG compression/decompression algorithm \cite{wallace1992jpeg} and introduce the multilinear method that we use to formulate our networks \cite{smith1994fast}.

\subsection{JPEG Compression}

The JPEG compression algorithm is defined as the following steps.

\begin{enumerate}
    \item Divide the image into $8 \times 8$ blocks
    \item Compute the 2D forward Discrete Cosine Transform (DCT Type 2) of each block
    \item Linearize the blocks using a zigzag order to produce a 64 component vector
    \item Element-wise divide each vector by a quantization coefficient
    \item Round the the vector elements to the nearest integer
    \item Run-length code and entropy code the vectors
\end{enumerate}

This process is repeated independently for each image plane. In most cases, the original image is transformed from the RGB color space to YUV and chroma subsampling is applied since the human visual system is less sensitive to small color changes than to small brightness changes \cite{winkler2001vision}. The decompression algorithm is the inverse process. Note that the rounding step (step 5) must be skipped during decompression. This is the step in JPEG compression where information is lost and is the cause of artifacts in decompressed JPEG images.

The magnitude of the information loss can be tuned using the quantization coefficients. If a larger coefficient is applied in step 4, then the result will be closer to 0 which increases its likelihood of being dropped altogether during rounding. In this way, the JPEG transform forces sparsity on the representation, which is why it compresses image data so well. This is coupled with the tendency of the DCT to push the magnitude of the coefficients into the upper left corner (the DC coefficient and the lowest spatial frequency) resulting in high spatial frequencies being dropped. Not only do these high spatial frequencies contribute less response to the human visual system, but they are also the optimal set to drop for a least squares reconstruction of the original image:

\begin{theorem}[DCT Least Squares Approximation Theorem]
    Given a set of $N$ samples of a signal $X = \{x_0, ... x_N\}$, let $Y = \{y_0, ... y_N\}$ be the DCT coefficients of $X$. Then, for any $1 \leq m \leq N$, the approximation
    
    \begin{equation}
    p_m(t) = \frac{1}{\sqrt{n}}y_o + \sqrt{\frac{2}{n}}\sum_{k=1}^{m} y_k\cos\left(\frac{k(2t + 1)\pi}{2n}\right)
    \end{equation}
    of $X$ minimizes the least squared error
    \begin{equation}
    e_m = \sum_{i=0}^{n} (p_m(i) - x_i)^2
    \end{equation}
    \label{thm:dctls}
\end{theorem}

Theorem \ref{thm:dctls} states that a reconstruction using the $m$ lowest spatial frequencies is optimal with respect to any other set of $m$ spatial frequencies. Proof of Theorem \ref{thm:dctls} is given in the supplementary material.

\subsection{JPEG Linear Map}

\label{sec:backjlm}

A key observation of the JPEG algorithm, and the foundation of most compressed domain processing methods \cite{chang1992video, chang1993new, natarajan1995fast, shen1995inner, shen1996direct, shen1998block, smith1993algorithms, smith1994fast} is that steps 1-4 of the JPEG compression algorithm are linear maps, so they can be composed, along with other linear operations, into a single linear map which performs the operations on the compressed representation. Step 5, the rounding step, is irreversible and ignored by decompression. Step 6, the entropy coding, is a nonlinear map and its form is computed from the data directly, so it is difficult to work with this representation. We define the JPEG Transform Domain as the output of Step 4 in the JPEG encoding algorithm. This is a standard convention of compressed domain processing. Inputs to the algorithms described here will be JPEGs after reversing the entropy coding.

Formally, we model a single plane image as the type (0, 2) tensor $I \in H^* \otimes W^*$ where $H$ and $W$ are vector spaces and $*$ denotes the dual space. We always use the standard orthonormal basis for these vector spaces which allows the free raising and lowering of indices without the use of a metric tensor.
We define the JPEG transform as the type (2, 3) tensor $J \in H \otimes W \otimes X^* \otimes Y^* \otimes K^*$. $J$ represents a linear map $J: H^* \otimes W^* \rightarrow X^* \otimes Y^* \otimes K^*$ and is computed as (in Einstein notation) 

\begin{equation}
I'_{xyk} = J^{hw}_{xyk}I_{hw}
\end{equation}
We say that $I'$ is the representation of $I$ in the JPEG transform domain. The indices $h,w$ give pixel position, $x,y$ give block position, and $k$ gives the offset into the block.

The form of $J$ is constructed from the JPEG compression steps listed in the previous section. Let the linear map $B: H^* \otimes W^* \rightarrow X^* \otimes Y^* \otimes M^* \otimes N^*$ be defined as 

\begin{equation}
B^{hw}_{xymn} = \left\{ \begin{array}{lr} 1 & \text{$h,w$ belongs in block $x,y$ at offset $m,n$} \\ 0 & \text{otherwise} \end{array} \right.
\end{equation} 
then $B$ can be used to break the image represented by $I$ into blocks of a given size such that the first two indices $x,y$ index the block position and the last two indices $m,n$ index the offset into the block.

Next, let the linear map $D: M^* \otimes N^* \rightarrow A^* \otimes B^*$ be defined as 

\begin{align}
D^{mn}_{\alpha\beta} = \frac{1}{4}V(\alpha)V(\beta)\cos\left(\frac{(2m+1)\alpha\pi}{16}\right)\cos\left(\frac{(2n+1)\beta\pi}{16}\right)
\end{align}
where $V(u)$ is a normalizing scale factor. Then $D$ represents the 2D discrete forward (and inverse) DCT. Let $Z: A^* \otimes B^* \rightarrow \Gamma^*$ be defined as 

\begin{equation}
Z^{\alpha\beta}_\gamma = \left\{ \begin{array}{lr} 1 & \text{$\alpha, \beta$ is at $\gamma$ under zigzag ordering} \\ 0 & \text{otherwise} \end{array} \right.
\end{equation}
then $Z$ creates the zigzag ordered vectors. Finally, let $S: \Gamma^* \rightarrow K^*$ be

\begin{equation}
    S^\gamma_k = \frac{1}{q_k}
\end{equation}
where $q_k$ is a quantization coefficient. This scales the vector entries by the quantization coefficients.

With linear maps for each step of the JPEG transform, we can then create the $J$ tensor described at the beginning of this section

\begin{equation}
J^{hw}_{xyk} = B^{hw}_{xymn}D^{mn}_{\alpha\beta}Z^{\alpha\beta}_{\gamma}S^\gamma_k
\end{equation}

The inverse mapping also exists as a tensor $\widetilde{J}$ which can be defined using the same linear maps with the exception of $S$. Let $\widetilde{S}$ be

\begin{equation}
\widetilde{S}^k_\gamma = q_k
\end{equation}
Then 

\begin{equation}
    \widetilde{J}^{xyk}_{hw} = B_{hw}^{xymn}D_{mn}^{\alpha\beta}Z_{\alpha\beta}^{\gamma}\widetilde{S}^k_\gamma
\end{equation}

Next consider a linear map $C: H^* \otimes W^* \rightarrow H^* \otimes W^*$ which performs an arbitrary pixel manipulation on an image plane $I$. To apply this mapping to a JPEG image $I'$, we first decompress the image, apply $C$ to the result, then compress that result to get the final JPEG. Since compressing is an application of $J$ and decompressing is an application of $\widetilde{J}$, we can form a new linear map $\Xi: X^* \otimes Y^* \otimes K^* \rightarrow X^* \otimes Y^* \otimes K^*$ as

\begin{equation}
\label{eq:stoj}
    \Xi^{xyk}_{x'y'k'} = \widetilde{J}^{xyk}_{hw}C^{hw}_{h'w'}J^{h'w'}_{x'y'k'}
\end{equation}
which applies $C$ in the JPEG transform domain. There are two important points to note about $\Xi$. The first is that, although it encapsulates decompression, applying $C$ and compressing, it uses far fewer operations than doing these processes separately since the coefficients are multiplied out. The second is that it is mathematically equivalent to performing $C$ on the decompressed image and compressing the result. It is not an approximation.