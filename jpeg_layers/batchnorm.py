import torch
from jpeg_codec import S_i


class BatchNorm(torch.nn.modules.Module):
    def __init__(self, bn):
        super(BatchNorm, self).__init__()

        self.register_buffer('running_mean', bn.running_mean.clone())
        self.register_buffer('running_var', bn.running_var.clone())
        self.register_buffer('S_i', S_i())

        self.momentum = bn.momentum
        self.eps = bn.eps

        self.gamma = torch.nn.Parameter(bn.weight.clone())
        self.beta = torch.nn.Parameter(bn.bias.clone())

    def forward(self, input):
        if self.training:
            channels = input.shape[1]

            input_channelwise = input.permute(1, 0, 2, 3, 4).clone()

            # Compute the batch mean for each channel
            block_means = input_channelwise[:, :, :, :, 0].contiguous().view(channels, -1)
            batch_mean = torch.mean(block_means, 1)

            # Compute the batch variance for each channel
            input_dequantized = torch.einsum('mtxyk,gk->mtxyg', [input_channelwise, self.S_i])
            input_dequantized[:, :, :, :, 0] = 0  # zero mean
            block_variances = torch.mean(input_dequantized**2, 4).view(channels, -1)
            batch_var = torch.mean(block_variances + block_means**2, 1) - batch_mean**2

            # Apply bessel correction to match pytorch i dont think this is really necessary
            bessel_correction_factor = input.shape[0] * input.shape[2] * input.shape[3] * 64
            bessel_correction_factor = bessel_correction_factor / (bessel_correction_factor - 1)
            batch_var *= bessel_correction_factor
            batch_var = batch_var

            # Update running stats
            self.running_mean = self.running_mean * (1 - self.momentum) + batch_mean * self.momentum
            self.running_var = self.running_var * (1 - self.momentum) + batch_var * self.momentum

            # Apply parameters
            invstd = 1. / torch.sqrt(batch_var + self.eps).view(1, -1, 1, 1, 1)
            mean = batch_mean.view(1, -1, 1, 1)
        else:
            invstd = 1. / torch.sqrt(self.running_var + self.eps).view(1, -1, 1, 1, 1)
            mean = self.running_mean.view(1, -1, 1, 1)

        g = self.gamma.view(1, -1, 1, 1, 1)
        b = self.beta.view(1, -1, 1, 1)

        input[:, :, :, :, 0] = input[:, :, :, :, 0] - mean
        input = input * invstd
        input = input * g
        input[:, :, :, :, 0] = input[:, :, :, :, 0] + b

        return input
