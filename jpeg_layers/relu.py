import torch
import opt_einsum as oe
from jpeg_codec import D_n, D, Z, S_i, S
from jpeg_codec import encode


class ASMReLU(torch.nn.modules.Module):
    def __init__(self, n_freqs):
        super(ASMReLU, self).__init__()
        C_n = torch.einsum('ijab,abg,gk->ijk', [D_n(n_freqs), Z(), S_i()])
        self.register_buffer('C_n', C_n)

        Hm = torch.einsum('ijab,ijuv,abg,gk,uvh,hl->ijkl', [D(), D(), Z(), S_i(), Z(), S()])
        self.register_buffer('Hm', Hm)

        self.make_masking_ops()

    def make_masking_ops(self):
        self.annm_op = oe.contract_expression('ijk,tmxyk->tmxyij', self.C_n, [0, 0, 0, 0, 64], constants=[0], optimize='optimal')
        self.annm_op.evaluate_constants(backend='torch')

        self.hsm_op = oe.contract_expression('ijkl,tmxyk,tmxyij->tmxyl', self.Hm, [0, 0, 0, 0, 64], [0, 0, 0, 0, 8, 8], constants=[0], optimize='optimal')
        self.hsm_op.evaluate_constants(backend='torch')

    def _apply(self, fn):
        s = super(ASMReLU, self)._apply(fn)
        s.make_masking_ops()
        return s

    def annm(self, x):
        appx_im = self.annm_op(x, backend='torch')
        mask = torch.zeros_like(appx_im)
        mask[appx_im >= 0] = 1
        return mask

    def half_spatial_mask(self, x, m):
        return self.hsm_op(x, m, backend='torch')

    def forward(self, input):
        annm = self.annm(input)
        out_comp = self.half_spatial_mask(input, annm)
        return out_comp


class APXReLU(torch.nn.modules.Module):
    def __init__(self, n_freqs):
        super(APXReLU, self).__init__()
        C_n = torch.einsum('ijab,abg,gk->ijk', [D_n(n_freqs), Z(), S_i()])
        self.register_buffer('C_n', C_n)

        C = torch.einsum('ijab,abg,gk->ijk', [D_n(n_freqs), Z(), S()])
        self.register_buffer('C', C)

        self.make_masking_ops()

    def make_masking_ops(self):
        self.annm_op = oe.contract_expression('ijk,tmxyk->tmxyij', self.C_n, [0, 0, 0, 0, 64], constants=[0], optimize='optimal')
        self.annm_op.evaluate_constants(backend='torch')

        self.compress_op = oe.contract_expression('ijk,tmxyij->tmxyk', self.C, [0, 0, 0, 0, 8, 8], constants=[0], optimize='optimal')
        self.compress_op.evaluate_constants(backend='torch')

    def _apply(self, fn):
        s = super(APXReLU, self)._apply(fn)
        s.make_masking_ops()
        return s

    def appx_relu(self, x):
        appx_im = self.annm_op(x, backend='torch')
        appx_im[appx_im < 0] = 0
        return appx_im

    def forward(self, input):
        appx = self.appx_relu(input)
        appx = self.compress_op(appx, backend='torch')
        return appx
