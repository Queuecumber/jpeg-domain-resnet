import torch
import opt_einsum as oe
import numpy as np


class Conv2d(torch.nn.modules.Module):
    def __init__(self, conv_spatial, J):
        super(Conv2d, self).__init__()

        self.stride = conv_spatial.stride
        self.padding = conv_spatial.padding
        self.weight = torch.nn.Parameter(conv_spatial.weight.clone())

        self.register_buffer('J', J[0])
        self.register_buffer('J_i', J[1])

        J_batched = self.J_i.contiguous().view(np.prod(self.J_i.shape[0:3]), 1, *self.J_i.shape[3:5])
        self.register_buffer('J_batched', J_batched)

        self.make_apply_op()

        self.jpeg_op = None

    def make_apply_op(self):
        input_shape = [0, self.weight.shape[1], *self.J_i.shape[0:3]]
        jpeg_op_shape = [self.weight.shape[0], self.weight.shape[1], *self.J_i.shape[0:3], *self.J.shape[0:2]]

        self.apply_conv = oe.contract_expression('mnxyksr,srabc,tnxyk->tmabc',  jpeg_op_shape, self.J, input_shape, constants=[1], optimize='optimal')
        self.apply_conv.evaluate_constants(backend='torch')

    def _apply(self, fn):
        s = super(Conv2d, self)._apply(fn)
        s.make_apply_op()
        return s

    def explode(self):
        out_channels = self.weight.shape[0]
        in_channels = self.weight.shape[1]

        jpeg_op = torch.nn.functional.conv2d(self.J_batched, self.weight.view(out_channels * in_channels, 1, self.weight.shape[2], self.weight.shape[3]), padding=self.padding, stride=self.stride)
        jpeg_op = jpeg_op.permute(1, 0, 2, 3)
        jpeg_op = jpeg_op.view(out_channels, in_channels, *self.J_i.shape[0:3], *(np.array(self.J_i.shape[3:5]) // self.stride))

        return jpeg_op

    def explode_pre(self):
        self.jpeg_op = self.explode()

    def forward(self, input):
        if self.jpeg_op is not None:
            jpeg_op = self.jpeg_op
        else:
            jpeg_op = self.explode()

        return self.apply_conv(jpeg_op, input, backend='torch')
