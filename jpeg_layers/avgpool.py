import torch


class AvgPool(torch.nn.modules.Module):
    def __init__(self):
        super(AvgPool, self).__init__()

    def forward(self, input):
        result = torch.mean(input[:, :, :, :, 0].view(-1, input.shape[1], input.shape[2]*input.shape[3]), 2)
        return result
