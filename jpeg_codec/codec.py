import torch
import numpy as np
from .tensors import D, Z, S, S_i, B

C = torch.einsum('ijab,abg,gk->ijk', (D(), Z(), S()))
C_i = torch.einsum('ijab,abg,gk->ijk', (D(), Z(), S_i()))


def codec(image_size, block_size=(8, 8)):
    B_i = B(image_size, block_size)
    J = torch.einsum('srxyij,ijk->srxyk', (B_i, C))
    J_i = torch.einsum('srxyij,ijk->xyksr', (B_i, C_i))
    return J, J_i


def encode(batch, block_size=(8, 8), device=None):
    J, _ = codec(batch.shape[2:], block_size)

    if device is not None:
        batch = batch.to(device)
        J = J.to(device)

    jpeg_batch = torch.einsum('srxyk,ncsr->ncxyk', (J, batch))
    return jpeg_batch


def decode(batch, device=None):
    block_size = int(np.sqrt(batch.shape[4]))
    image_size = (int(batch.shape[2] * block_size), int(batch.shape[3] * block_size))

    _, J_i = codec(image_size, (block_size, block_size))

    if device is not None:
        batch = batch.to(device)
        J_i = J_i.to(device)

    spatial_batch = torch.einsum('xyksr,ncxyk->ncsr', (J_i, batch))
    return spatial_batch
