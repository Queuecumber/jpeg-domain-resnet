import numpy as np
import torch
import os


def write_jpeg(data, labels, directory, fname):
    os.makedirs(directory, exist_ok=True)
    path = '{}/{}'.format(directory, fname)
    with open(path, 'wb') as f:
        np.savez(f, data=data.cpu().numpy(), labels=labels.cpu().numpy())


def load_jpeg(directory, fname):
    path = '{}/{}'.format(directory, fname)

    with open(path, 'rb') as f:
        dl = np.load(f)
        data = dl['data']
        labels = dl['labels']

    return torch.Tensor(data), torch.LongTensor(labels)
