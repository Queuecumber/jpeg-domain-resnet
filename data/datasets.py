import torch
from torchvision import datasets, transforms
from .jpeg_dataset import JpegDataset


def spatial_data(batch_size, root, name, dataset, transform, shuffle_train):
    train_data = dataset('{}/{}-data'.format(root, name), train=True, download=True, transform=transform)
    test_data = dataset('{}/{}-data'.format(root, name), train=False, download=True, transform=transform)

    train_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size, shuffle=shuffle_train)
    test_loader = torch.utils.data.DataLoader(test_data, batch_size=batch_size, shuffle=False)

    return train_loader, test_loader


def mnist_spatial(batch_size, root, shuffle_train=True):
    transform = transforms.Compose([
        transforms.Pad(2),
        transforms.ToTensor(),
        transforms.Normalize((0.1307,), (0.3081,))
    ])

    directory = '{}/{}'.format(root, 'MNIST')
    return spatial_data(batch_size=batch_size, root=directory, name='MNIST-spatial', dataset=datasets.MNIST, transform=transform, shuffle_train=shuffle_train)


def cifar10_spatial(batch_size, root, shuffle_train=True):
    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.4914, 0.4822, 0.4465), (0.247, 0.243, 0.261))
    ])

    directory = '{}/{}'.format(root, 'CIFAR10')
    return spatial_data(batch_size=batch_size, root=directory, name='CIFAR10-spatial', dataset=datasets.CIFAR10, transform=transform, shuffle_train=shuffle_train)


def cifar100_spatial(batch_size, root, shuffle_train=True):
    transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))
    ])

    directory = '{}/{}'.format(root, 'CIFAR100')
    return spatial_data(batch_size=batch_size, root=directory, name='CIFAR100-spatial', dataset=datasets.CIFAR100, transform=transform, shuffle_train=shuffle_train)


def jpeg_data(batch_size, directory, shuffle_train):
    train_data = JpegDataset(directory=directory, fname='train.npz')
    test_data = JpegDataset(directory=directory, fname='test.npz')

    train_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size, shuffle=shuffle_train)
    test_loader = torch.utils.data.DataLoader(test_data, batch_size=batch_size, shuffle=False)

    return train_loader, test_loader


def mnist_jpeg(batch_size, root, shuffle_train=True):
    directory = '{}/{}'.format(root, 'MNIST')
    return jpeg_data(batch_size, directory, shuffle_train)


def cifar10_jpeg(batch_size, root, shuffle_train=True):
    directory = '{}/{}'.format(root, 'CIFAR10')
    return jpeg_data(batch_size, directory, shuffle_train)


def cifar100_jpeg(batch_size, root, shuffle_train=True):
    directory = '{}/{}'.format(root, 'CIFAR100')
    return jpeg_data(batch_size, directory, shuffle_train)


spatial_dataset_map = {
    'MNIST': mnist_spatial,
    'CIFAR10': cifar10_spatial,
    'CIFAR100': cifar100_spatial
}

jpeg_dataset_map = {
    'MNIST': mnist_jpeg,
    'CIFAR10': cifar10_jpeg,
    'CIFAR100': cifar100_jpeg
}

dataset_info = {
    'MNIST': {
        'channels': 1,
        'classes': 10
    },
    'CIFAR10': {
        'channels': 3,
        'classes': 10
    },
    'CIFAR100': {
        'channels': 3,
        'classes': 100,
    }
}
