import argparse
from .datasets import *
from .jpeg_readwrite import *
from jpeg_codec import encode

parser = argparse.ArgumentParser()
parser.add_argument('--batch_size', type=int, help='Number of images to convert at a time', default=60000)
parser.add_argument('dataset', choices=spatial_dataset_map.keys(), help='Dataset to convert')
parser.add_argument('directory', help='directory to load and store datasets')
args = parser.parse_args()

print(args)

train_data, test_data = spatial_dataset_map[args.dataset](args.batch_size, root=args.directory, shuffle_train=False)

device = torch.device('cuda')

print('Encoding training data...')
jpeg_data = []
jpeg_label = []
for data, label in train_data:
    jpeg_data.append(encode(data, device=device))
    jpeg_label.append(label)

print('Writing training data...')

jpeg_data = torch.cat(jpeg_data)
jpeg_label = torch.cat(jpeg_label)
write_jpeg(jpeg_data, jpeg_label, args.directory, 'train.npz')

print('Encoding testing data...')
jpeg_data = []
jpeg_label = []
for data, label in test_data:
    jpeg_data.append(encode(data, device=device))
    jpeg_label.append(label)

print('Writing testing data...')
jpeg_data = torch.cat(jpeg_data)
jpeg_label = torch.cat(jpeg_label)
write_jpeg(jpeg_data, jpeg_label, args.directory, 'test.npz')
