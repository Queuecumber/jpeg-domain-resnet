import torch
from .jpeg_readwrite import load_jpeg


class JpegDataset(torch.utils.data.Dataset):
    def __init__(self, directory, fname):
        self.data, self.labels = load_jpeg(directory, fname)

    def __len__(self):
        return self.data.shape[0]

    def __getitem__(self, idx):
        return self.data[idx], self.labels[idx]
