import argparse
import jpeg_layers
import torch
import torch.nn
import numpy as np
import opt_einsum as oe
from jpeg_codec import D_n, Z, S_i, encode, decode

device = torch.device('cpu')


def rmse_error(a, b):
    return torch.sqrt(torch.mean((a - b)**2))


def double_size_tensor(shape):
    op = torch.zeros((shape[0], shape[1], shape[0] * 2, shape[1] * 2)).to(device)
    for i in range(0, shape[0]):
        for j in range(0, shape[1]):
            for u in range(0, shape[0] * 2):
                for v in range(0, shape[1] * 2):
                    if i == u // 2 and j == v // 2:
                        op[i, j, u, v] = 1

    return op


doubling_tensor = double_size_tensor((4, 4))

parser = argparse.ArgumentParser()
parser.add_argument('--batch_size', type=int, help='Number of samples per batch')
parser.add_argument('--batches', type=int, help='Number of batches')
parser.add_argument('--output', help='Output CSV file name')
args = parser.parse_args()

annm_errors = np.zeros(15)
appx_errors = np.zeros(15)

spatial_relu = torch.nn.ReLU(inplace=False).to(device)

for f in range(15):
    print('Processsing spatial frequency {}'.format(f))
    jpeg_relu = jpeg_layers.ASMReLU(f).to(device)
    appx_relu = jpeg_layers.APXReLU(f).to(device)

    for b in range(args.batches):
        im = torch.rand(args.batch_size, 1, 8, 8).to(device) * 2 - 1
        #im = torch.einsum('ijuv,ncij->ncuv', [doubling_tensor, im])

        im_jpeg = encode(im, device=device)

        true_relu = spatial_relu(im)
        annm_relu = jpeg_relu(im_jpeg)
        apx_relu = appx_relu(im_jpeg)

        annm_im = decode(annm_relu, device=device)
        apx_im = decode(apx_relu, device=device)

        annm_errors[f] += rmse_error(annm_im, true_relu)
        appx_errors[f] += rmse_error(apx_im, true_relu)

annm_errors /= args.batches
appx_errors /= args.batches

with open(args.output, 'w') as f:
    f.write('ASM,APX\n')
    for i in range(15):
        f.write('{},{}\n'.format(annm_errors[i], appx_errors[i]))

