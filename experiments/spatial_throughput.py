import models
import torch
import torch.optim as optim
import argparse
import data
import time

device = torch.device('cuda')
torch.backends.cudnn.enabled = False

parser = argparse.ArgumentParser()
parser.add_argument('--dataset', choices=data.spatial_dataset_map.keys(), help='Dataset to use')
parser.add_argument('--batch_size', type=int, help='Batch size')
parser.add_argument('--data', help='Root folder for the dataset')
args = parser.parse_args()

dataset = data.jpeg_dataset_map[args.dataset](args.batch_size, args.data)
dataset_info = data.dataset_info[args.dataset]
spatial_model = models.SpatialResNet(dataset_info['channels'], dataset_info['classes']).to(device)
optimizer = optim.Adam(spatial_model.parameters())


t0 = time.time()
models.train(spatial_model, device, dataset[0], optimizer, 0, do_decode=True)
torch.cuda.synchronize()
t1 = time.time()

training_time = t1 - t0

t0 = time.time()
models.test(spatial_model, device, dataset[1], do_decode=True)
torch.cuda.synchronize()
t1 = time.time()
testing_time = t1 - t0

with open('{}_spatial_throughput.csv'.format(args.dataset), 'w') as f:
    f.write('Training, Testing\n')
    f.write('{}, {}\n'.format(len(dataset[0]) / training_time, len(dataset[1]) / testing_time ))
