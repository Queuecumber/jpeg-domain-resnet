import models
import torch
import torch.optim as optim
import argparse
import data
import time

device = torch.device('cuda')

parser = argparse.ArgumentParser()
parser.add_argument('--dataset', choices=data.spatial_dataset_map.keys(), help='Dataset to use')
parser.add_argument('--batch_size', type=int, help='Batch size')
parser.add_argument('--data', help='Root folder for the dataset')
args = parser.parse_args()

dataset = data.jpeg_dataset_map[args.dataset](args.batch_size, args.data)
dataset_info = data.dataset_info[args.dataset]
spatial_model = models.SpatialResNet(dataset_info['channels'], dataset_info['classes'])
jpeg_model = models.JpegResNet(spatial_model, n_freqs=6).to(device)
optimizer = optim.Adam(jpeg_model.parameters())


t0 = time.time()
models.train(jpeg_model, device, dataset[0], optimizer, 0)
torch.cuda.synchronize()
t1 = time.time()

training_time = t1 - t0

jpeg_model.explode_all()

t0 = time.time()
models.test(jpeg_model, device, dataset[1])
torch.cuda.synchronize()
t1 = time.time()
testing_time = t1 - t0

with open('{}_jpeg_throughput.csv'.format(args.dataset), 'w') as f:
    f.write('Training, Testing\n')
    f.write('{}, {}\n'.format(len(dataset[0]) / training_time, len(dataset[1]) / testing_time))
