import torch.nn as nn
from .blocks import JpegResBlock
from jpeg_layers import AvgPool, ASMReLU, APXReLU
from jpeg_codec import codec


class JpegResNet(nn.Module):
    def __init__(self, spatial_model, n_freqs, relu_layer=ASMReLU):
        super(JpegResNet, self).__init__()

        J_32 = codec((32, 32))
        J_16 = codec((16, 16))
        J_8 = codec((8, 8))

        self.block1 = JpegResBlock(spatial_model.block1, n_freqs=n_freqs, J_in=J_32, J_out=J_32, relu_layer=relu_layer)
        self.block2 = JpegResBlock(spatial_model.block2, n_freqs=n_freqs, J_in=J_32, J_out=J_16, relu_layer=relu_layer)
        self.block3 = JpegResBlock(spatial_model.block3, n_freqs=n_freqs, J_in=J_16, J_out=J_8, relu_layer=relu_layer)

        self.averagepooling = AvgPool()
        self.fc = spatial_model.fc

    def explode_all(self):
        self.block1.explode_all()
        self.block2.explode_all()
        self.block3.explode_all()

    def forward(self, x):
        out = self.block1(x)
        out = self.block2(out)
        out = self.block3(out)

        out = self.averagepooling(out)
        out = out.view(x.size(0), -1)

        out = self.fc(out)

        return out


class JpegResNetExact(JpegResNet):
    def __init__(self, spatial_model):
        super(JpegResNetExact, self).__init__(spatial_model, 14)


class JpegResNetApx(JpegResNet):
    def __init__(self, spatial_model, n_freqs):
        super(JpegResNetApx, self).__init__(spatial_model, n_freqs, APXReLU)
