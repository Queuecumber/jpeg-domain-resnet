import torch.nn as nn
import jpeg_layers


class SpatialResBlock(nn.Module):
    def __init__(self, in_channels, out_channels, downsample=True):
        super(SpatialResBlock, self).__init__()

        self.downsample = downsample

        stride = 2 if downsample else 1

        self.conv1 = nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=3, stride=stride, padding=1, bias=False)
        self.conv2 = nn.Conv2d(in_channels=out_channels, out_channels=out_channels, kernel_size=3, stride=1, padding=1, bias=False)

        self.bn1 = nn.BatchNorm2d(out_channels)
        self.bn2 = nn.BatchNorm2d(out_channels)
        self.relu = nn.ReLU(inplace=True)

        if downsample or in_channels != out_channels:
            stride = 2 if downsample else 1
            self.downsampler = nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=1, stride=stride, padding=0, bias=False)
            self.bn_ds = nn.BatchNorm2d(out_channels)
        else:
            self.downsampler = None

    def forward(self, x):
        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsampler is not None:
            residual = self.downsampler(x)
            residual = self.bn_ds(residual)
        else:
            residual = x

        out += residual
        out = self.relu(out)

        return out


class JpegResBlock(nn.Module):
    def __init__(self, spatial_resblock, n_freqs, J_in, J_out, relu_layer=jpeg_layers.ASMReLU):
        super(JpegResBlock, self).__init__()

        J_down = (J_out[0], J_in[1])

        self.conv1 = jpeg_layers.Conv2d(spatial_resblock.conv1, J_down)
        self.conv2 = jpeg_layers.Conv2d(spatial_resblock.conv2, J_out)

        self.bn1 = jpeg_layers.BatchNorm(spatial_resblock.bn1)
        self.bn2 = jpeg_layers.BatchNorm(spatial_resblock.bn2)

        self.relu = relu_layer(n_freqs=n_freqs)

        if spatial_resblock.downsampler is not None:
            self.downsampler = jpeg_layers.Conv2d(spatial_resblock.downsampler, J_down)
            self.bn_ds = jpeg_layers.BatchNorm(spatial_resblock.bn_ds)
        else:
            self.downsampler = None

    def explode_all(self):
        self.conv1.explode_pre()
        self.conv2.explode_pre()

        if self.downsampler is not None:
            self.downsampler.explode_pre()

    def forward(self, x):
        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsampler is not None:
            residual = self.downsampler(x)
            residual = self.bn_ds(residual)
        else:
            residual = x

        out += residual
        out = self.relu(out)
        return out
